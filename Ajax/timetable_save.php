<?php

    require_once(__DIR__."/../initialization.php");

    global $connection; 

    $username = $_SESSION["username"];
    $content = $_GET['content'];
    $id = $_GET['id'];

    // Getting loggedin user's timetable data with given id
    $res = mysqli_query($connection, "SELECT * FROM timetable
    WHERE username='$username' AND id='$id'") 
    or die($connection);

    // Alters existent data, if id was found in database
    if(mysqli_num_rows($res)) {
        
        // Empty and '.' content means entry was deleted
        if($content == "." || $content=="") {
            mysqli_query($connection, "DELETE FROM timetable
            WHERE username='$username' AND id='$id'") 
            or die($connection);
        } 
        
        // Data is updated 
        else {
            mysqli_query($connection, "UPDATE timetable
            SET content='$content' 
            WHERE username='$username' AND id='$id'") 
            or die($connection);
        }
    } 

    // Inserts new data, if id was not found in database
    else {
        mysqli_query($connection, "INSERT INTO timetable
        VALUES ('$username', '$content', '$id')") 
        or die($connection);
    }

    // Redirecting back
    go(__DIR__."../UI/timetable.php");