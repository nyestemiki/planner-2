<?php

    /**
     * This module is for the asyncronous addition of a new place at weather.php
     */

    require_once(__DIR__."/../initialization.php");

    global $connection;

    $user = $_SESSION['username'];
    $place = $_REQUEST['place'];

    // Operation class is created
    $operation = new weatherOperations($place);

    // Contains if new place could be created
    $state = $operation->new_place();

    // 0 error code: place not found
    if($state == 0) {

        // ResponseText is loaded
        echo 'Place not found';

        // Operation is aborted
        return;
    } 

    // 1 error code: place already in database
    else if($state == 1) {

        // ResponseText is loaded
        echo 'Already added';

        // Operation is aborted
        return;
    } 

    // No error code received
    else {

        // Getting data of place
        $weather = $operation->get_data();

        $place = $weather->get_place();
        $degree = $weather->get_degree();
        $description = $weather->get_description();

        // ResponseText is loaded
        echo ('
            <div class="weather btn" id="widget//'.$place.'">
                <span class="place">'.$place.'</span>
                <span class="degree">'.$degree.'</span>
                <span class="description">'.$description.'</span>
                <button onclick="del(this)" class="delete-weather" id="'.$place.'">Delete</button>
            </div>
        '); 
    }

    