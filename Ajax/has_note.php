<?php

    /**
     * This module is for determining whether a certain subject has notes
     */

    require_once(__DIR__."/../initialization.php");

    global $connection;

    $user = $_SESSION['username'];

    /* 
        Notes and subjects are in the same table. Subjects are initialized with empty note
        This means to determine whether a subject has notes, we need to see if there is anything
        else other than blank space for the given subject
    */

    // All of the users notes and subjects
    $content_all = mysqli_query($connection, "SELECT subject FROM notes 
    WHERE user='$user' AND note=''") or die($connection);

    // Only the users notes
    $content_notempty = mysqli_query($connection, "SELECT subject FROM notes 
    WHERE user='$user' AND note!=''") or die($connection);
    
    // Will contain subjects with note(s)
    $array_notempty = array();

    // For every note, the array of subjects with notes is altered
    for($i=0; $i<mysqli_num_rows($content_notempty); $i++){
        $array_notempty[] =  mysqli_fetch_assoc($content_notempty)["subject"];
    }

    // Subtracting the subjects with notes from all subjects gives the subjects with no note
    for($i=0; $i<mysqli_num_rows($content_all); $i++){
        $el = mysqli_fetch_assoc($content_all)["subject"];

        if(!in_array($el, $array_notempty))

            // responseText is loaded with subject
            echo $el.";"; 
    }