<?php

    /**
     * This module is for validation of the data at registering
     * 
     * If given data not found in database, input is valid
     */

    // Before registering means file is accessible for everyone
    $GUEST_ACCESS = true;

    require_once(__DIR__."/../initialization.php");

    $username = $_REQUEST['username'];

    // Passwords are protected with sha1 encryption
    $password = sha1($_REQUEST['password']);

    // Getting appearances of user in database
    $request = mysqli_query($connection, "SELECT username FROM authentication 
    WHERE username='$username' AND password='$password'") 
    or die($connection);
    
    // ResponseText is loaded
    echo mysqli_num_rows($request) ? "false" : "true";