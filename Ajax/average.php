<?php

    /**
     * This module is for the calculation of the average of notes of a subject.
     */

    require_once(__DIR__."/../initialization.php");

    global $connection;

    $user = $_SESSION['username'];
    $subject = $_REQUEST['subject']; 

    // Selecting directly the average from the database
    $content = mysqli_query($connection, "SELECT AVG(note) as average FROM notes 
    WHERE user='$user' AND subject='$subject' AND note!=''") or die($connection);
    
    $average = mysqli_fetch_assoc($content)["average"];  

    // Loading responseText
    echo $subject."'s average: ".$average;