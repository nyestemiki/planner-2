<?php

    // New session starts for each user, at the very first call of initialization
    session_start();

    // Functions
    require_once("Functions/go.php");
    require_once("Functions/load_lists.php");
    require_once("Functions/load_list.php");
    require_once("Functions/load_weather.php");
    require_once("Functions/load_timetable.php");
    require_once("Functions/get_list_abc.php");
    require_once("Functions/get_list_userdefined.php");
    require_once("Functions/get_list_date.php");
    require_once("Functions/get_lists_abc.php");
    require_once("Functions/get_lists_userdefined.php");
    require_once("Functions/get_lists_date.php");
    require_once("Functions/load_balance.php");
    require_once("Functions/load_balance_sheet.php");
    require_once("Functions/load_subjects.php");
    require_once("Functions/load_notes.php");

    // Classes
    require_once("Domain/user.php");
    require_once("Domain/list_item.php");
    require_once("Domain/onelist.php");
    require_once("Domain/lists.php");
    require_once("Domain/weather.php");
    require_once("Domain/purchase.php");

    // Operations
    require_once("Domain/Operations/authenticationOperations.php");
    require_once("Domain/Operations/settingsOperations.php");
    require_once("Domain/Operations/listsOperations.php");
    require_once("Domain/Operations/listOperations.php");
    require_once("Domain/Operations/weatherOperations.php");
    require_once("Domain/Operations/balanceOperations.php");
    require_once("Domain/Operations/balanceSheetOperations.php");
    require_once("Domain/Operations/subjectsOperations.php");

    // Database
    require_once("configuration.php");
    require_once("dbconnect.php");
    require_once("dbinit.php");

    // Constraints
    require_once("constraints.php");