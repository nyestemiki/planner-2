<?php

    /**
     * This module is the guard of the page
     * 
     * Allows and prohibits access to pages
     */

    // If no user is logged in (access denied/ false) and guest access denied
    if($_SESSION["access"] == false && !isset($GUEST_ACCESS)) 

        // User is redirected to start page
        go("/UI/authentication.php");


    // Initializing accessibility variables
    if(!isset($_SESSION["access"])) 
        $_SESSION["access"] = false;

    // If logged in, no access to authentication page
    if(isset($page) && isset($_SESSION["username"])) 
        go("menu.php");