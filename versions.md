### Version 1: AUTHENTICATION
    Authentication initialized

### Version 1.0.1:
    Working authentication and registration with database

### Version 1.0.2:
    Functional authentication system
        - Pages with no $GUEST_ACCESS set true redirect if no logged in user

    Responsive design

    Auto-login after registering

### Version 1.1.0: LISTS
    Lists initialized

### Version 1.1.1: MENU
    Menu initialized

    Lists tile
        User's lists

    Bug fixed
        Logged in user can not access authentication page

    Detailed version description added

    Constraints and initialization separated
        initialization contains initialization of files
        constraints contain redirections (for the time being)

    Home page is now the menu

### Version 1.1.2:
    Delete list item

    Bug fixes:
        Lists displayed only once
        Encoding errors

### Version 1.2: ADD/DELETE
    Add/delete lists and list elements

    Style improvements

### Version 1.2.1: LIST NAME CHANGE
    List name is changable
    
### Version 1.2.2:
    Prompt for deleting lists

    Style improvements

### Version 1.2.3:
    Starrable list elements

### Version 1.2.4:
    Done/undone list elements

### Version 1.3: SETTINGS
    Settings added
        changeable username
        changeable password
        themes initialized

### Version 1.3.1: DELETE ACCOUNT
    Deletable accounts

### Version 1.3.2: 
    Responsive design fixes

    Auto hiding password-show button

    Find class (for given username)

    Error messages

### Version 1.3.3:
    Merge lists

### Version 1.3.4:
    Star lists

### Version 1.4: WEATHER
    Weather initialized
        User can add new places
        Accepting only those that exist
        Getting their weather data

### Version 1.4.1:
    Weather with degree and description
    Deletable weather widgets

### Version 1.4.2:
    List element rearranging initialized

### Version 1.4.3:
    Bug fix
        up first list item
        down last list item

    Refactoring
        load list (instead of getting all lists and selecting the one
        neede, getting only the one needed)

### Version 1.4.4:
    List elements order by
        name
        date
        userdefined

### Version 1.4.5:
    List element order menu

### Version 1.5: TIMETABLE
    Timetable initialized

### Version 1.4.6:
    Timetable 
        render bug fixed
        inputs saved in database

### Version 1.5: BALANCE
    Balance initialized

### Version 1.5.1:
    Balance sheets added
    Minor bug fixes
        Error messages

### Version 1.5.2:
    Lists order by name, date and userdefined

### Version 1.6: NOTES
    Subjects and notes initialied

### Version 1.6.1:
    Deletable notes and subjects

### Version 1.6.2:
    Delete confirm dialogue for subjects

### Version 1.6.3:
    Average for subjects

### Version 1.6.4:
    Database, tables auto-create if not exist
    Design ideas added

### Version 1.7: POMODORO
    Pomodoro timer added

### Version 1.7.1:
    Pomodoro Progress Animation
        CSS progressbar on percentage

### Version 1.7.2:
    Database optimization
        Empty timetable content is deleted instead of being stored
    Same list item names multiple times possible
        using ids instead of names

### Version 1.7.3:
    Ajax applied on weather

### Version 1.7.4:
    Bug fixes
        Error messages on weather
        Adds distinct (places)
        Style improvements
            Showing new place after error message (instead of typed value)
            Emptying textarea on click

### Version 1.7.5:
    Ajaxized deletion

### Version 1.7.5.1:
    Weather style improvements
        Button instead of state change

### Version 1.7.5.2:
    Timetable id's optimized
        buttons start with b//
        textareas start with t//
    Timetable style changes
        textarea is empty if button is '.'

### Version 1.8:
    Bug fixes
        Previously only one error would render at weather

    Timetable
        Edit mode
        Page opens to done mode

    !!! WEATHER COMPLETE

    !!! TIMETABLE COMPLETE

    !!! POMODORO COMPLETE

### Version 1.8.1:
    Authentification validation ajaxized

    Bug fixes
    Cleaner code
        Controllers cleaned up

    !!! AUTHENTICATION COMPLETE

### Version 1.8.2:
    Bug fixes
        List name change to empty
        Timetable unique ids destroyed


### Version 1.9 untested
        Completely refactored and commented

### TEST
    Lists tested
    Weather tested
    Timetable tested
    Notes tested
    Notes tested
    Pomodoro tested
    Settings tested

    All tests were successful and all applictions are working properly



### PLANNER 2 - VERSION 2 (tested) 