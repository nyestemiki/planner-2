<?php
    global $connection;
    
    /**
     * Array with connection data
     */
    $configuration = $config['db'];

    /**
     * Connects to database
     */
    $connection = mysqli_connect(
        $configuration['host'], 
        $configuration['user'], 
        $configuration['pass'], 
        $configuration['db']
    ) or die($connection);