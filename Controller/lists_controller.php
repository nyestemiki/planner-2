<?php

    require_once("../initialization.php");

    $mode = $_REQUEST["mode"];
    $list = $_REQUEST["list"];

    // Building operation class
    $operation = new listsOperations($list);

    // Adding new list
    if(isset($_REQUEST["add"])) $operation->add();
    // Deleting given list
    if(isset($_REQUEST["delete"])) $operation->delete();
    // Staring/unstaring selected list
    if(isset($_REQUEST["star"])) $operation->star();
    // Modifying selected list's name
    if(isset($_REQUEST["modify"])) $operation->modify($_REQUEST["new"]);
    // Merging two lists
    if(isset($_REQUEST["merge"])) $operation->merge($_REQUEST["new"]);
    // Moving selected list up by one position
    if(isset($_REQUEST["up"])) $operation->up();
    // Moving selected list down by one position
    if(isset($_REQUEST["down"])) $operation->down();

    // Resetting error variables if decline button was pressed
    if(isset($_REQUEST["nomerge"])) {
        $_SESSION["error_msg"] = null;
        $_SESSION["merge_conflict_new"] = null;
        $_SESSION["merge_conflict_list"] = null;
    }
    
    // Redirecting back after finished operation
    go("../UI/lists.php?mode=$mode");