<?php

    require_once("../initialization.php");
    
    // Building operation class
    $operation = new balanceOperations($_REQUEST["name"]);

    // Adding new category
    if(isset($_REQUEST["add"])) $operation->add();
    // Deleting given category
    if(isset($_REQUEST["delete"])) $operation->delete();

    // Redirecting back after finished operation
    go("../UI/balance.php");