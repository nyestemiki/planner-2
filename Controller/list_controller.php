<?php

    require_once("../initialization.php");

    $list = $_REQUEST["list"];
    $element = $_REQUEST["element"];
    $mode = $_REQUEST["mode"];

    // Building operation class
    $operation = new listOperations($list, $element);

    // Adding new list item
    if(isset($_REQUEST["add"])) $operation->add();
    // Deleting selected list item
    if(isset($_REQUEST["delete"])) $operation->delete();
    // Staring/unstaring selected list item
    if(isset($_REQUEST["star"])) $operation->star();
    // Overscoring/unoverscoring selected list item
    if(isset($_REQUEST["state"])) $operation->state();
    // Moving selected list item up by one position
    if(isset($_REQUEST["up"])) $operation->up();
    // Moving selected list item down by one position
    if(isset($_REQUEST["down"])) $operation->down();

    // Redirecting back, after operation finished
    go("../UI/list.php?list=$list&mode=$mode");