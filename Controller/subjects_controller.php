<?php

    require_once("../initialization.php");
    
    // Building operation class
    $operation = new subjectsOperations($_REQUEST["subject"]);

    // Adding new subject
    if(isset($_REQUEST["new_subject"])) $operation->new_subject();
    // Deleting selected subject
    if(isset($_REQUEST["delete_subject"])) $operation->delete_subject();
    // Adding new note to selected subject
    if(isset($_REQUEST['addnote'])) $operation->add_note($_REQUEST['note']);
    // Deleting selected note
    if(isset($_REQUEST["delete_note"])) $operation->delete_note($_REQUEST['note']);

    // Redirecting back after finished operation
    go("../UI/subject.php");