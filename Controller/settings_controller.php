<?php

    require_once("../initialization.php");
    
    // Building operation class
    $operation = new settingsOperations();

    // Changing loggedin user's name to given name
    if(isset($_REQUEST["usname"])) $operation->change_username($_REQUEST["newname"]);
    // Changing loggedin user's password to given password
    if(isset($_REQUEST["pswd"])) $operation->change_password($_REQUEST["newpass"]);
    // Deleting loggedin user
    if(isset($_REQUEST["delete"])) $operation->delete();

    // Redirecting back after finished operation
    go("../UI/settings.php");