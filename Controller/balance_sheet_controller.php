<?php

    require_once("../initialization.php");
    
    $category = $_REQUEST["category"];
    $content = $_REQUEST["content"];
    $price = $_REQUEST["price"];

    // Building operation class
    $operation = new balanceSheetOperations($content, $price, $category);

    // Adding new purchase
    if(isset($_REQUEST["add"])) $operation->add();
    // Deletinging selected purchase
    if(isset($_REQUEST["delete"]))  $operation->delete();

    // Redirecting back after finished operation
    go("../UI/balance_sheet.php?category=$category");