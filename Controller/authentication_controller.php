<?php

    require_once("../initialization.php");

    $username = $_REQUEST['username'] | '';
    $password = $_REQUEST['password'] | '';

    // Building user
    $user = new User($username, $password);
    // Building operation class
    $auth = new authenticationOperations($user);

    // Logging in user
    if(isset($_REQUEST["login"])) $auth->login();
    // Registering user
    else if(isset($_REQUEST["register"])) $auth->register();
    // Logging out user, if logout button was pressed
    else if(isset($_REQUEST["logout"])) $auth->logout();

    // Redirecting to menu after operation
    go("/UI/menu.php");