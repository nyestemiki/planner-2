### Dear curious visitor!
    This is a php based web application for organizing your life, focused on the backend. This explains the style and design. Nevertheless, it, or a future copy of it, will get some frontend, too.
    
    For less refreshes and redirects I approached some features with AJAX. However, I found it not so important to continue so. One the one hand, it would not add to much value to the app (as far as ux goes, it runs fast enough; as far as developement goes it's pure repetition). One the other hand, I found it obselete and will redo the whole app in React. (The drawback may occur when navigating with the arrows as a non-developer)

    Future versions will contain a built-in messenger app and a calendar.

    Designwise: on scroll the middle app from the illustration (design.png) comes into the centre, gets bigger and straight with a parallax effect. The user can navigate (left and right) between previews of the supported apps. Everything looks like a plan, the background should be a blueprint.