<?php

    require_once(__DIR__."/initialization.php");

    global $connection;

    /**
     * Creates database, if there is no 
     */
    mysqli_query($connection, "CREATE DATABASE IF NOT EXISTS planner2") 
    or die(mysqli_error($connection));

    /**
     * Creating nonexistent tables
     */
    mysqli_query($connection, "CREATE TABLE IF NOT EXISTS authentication (
        username VARCHAR(50) NOT NULL,
        password VARCHAR(50) NOT NULL,
        UNIQUE KEY (username)
    )") or die(mysqli_error($connection));

    mysqli_query($connection, "CREATE TABLE IF NOT EXISTS balance (
        user VARCHAR(100) NOT NULL,
        category VARCHAR(100) NOT NULL
    )") or die(mysqli_error($connection));

    mysqli_query($connection, "CREATE TABLE IF NOT EXISTS balance_sheet (
        user VARCHAR(100) NOT NULL,
        category VARCHAR(100) NOT NULL,
        content VARCHAR(300) NOT NULL,
        price VARCHAR(50) NOT NULL
    )") or die(mysqli_error($connection));

    mysqli_query($connection, "CREATE TABLE IF NOT EXISTS lists (
        list_name VARCHAR(25) NOT NULL,
        username VARCHAR(25) NOT NULL,
        item VARCHAR(100) CHARACTER SET utf8 NOT NULL,
        number float NOT NULL,
        status int(1) NOT NULL DEFAULT '0',
        starred int(1) NOT NULL DEFAULT '0',
        date timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
        item_id INT(10) NOT NULL AUTO_INCREMENT,
        UNIQUE KEY (item_id)
    )") or die(mysqli_error($connection));

    mysqli_query($connection, "CREATE TABLE IF NOT EXISTS list_order (
        list_name VARCHAR(50) NOT NULL,
        user VARCHAR(50) NOT NULL,
        id VARCHAR(11) NOT NULL DEFAULT '0'
    )") or die(mysqli_error($connection));

    mysqli_query($connection, "CREATE TABLE IF NOT EXISTS notes (
        user VARCHAR(100) NOT NULL,
        subject VARCHAR(100) NOT NULL,
        note VARCHAR(10) NOT NULL
    )") or die(mysqli_error($connection));

    mysqli_query($connection, "CREATE TABLE IF NOT EXISTS starred_lists (
        list_name VARCHAR(50) NOT NULL,
        user VARCHAR(25) NOT NULL
    )") or die(mysqli_error($connection));

    mysqli_query($connection, "CREATE TABLE IF NOT EXISTS timetable (
        username VARCHAR(25) NOT NULL,
        content VARCHAR(100) NOT NULL DEFAULT '.',
        id int(10) NOT NULL
    )") or die(mysqli_error($connection));

    mysqli_query($connection, "CREATE TABLE IF NOT EXISTS weather (
        user VARCHAR(50) NOT NULL,
        place VARCHAR(50) NOT NULL
    )") or die(mysqli_error($connection));