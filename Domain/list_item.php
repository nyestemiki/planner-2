<?php

    class List_item {
        
        /**
        * Item text
        *
        * @var [string] $item
        */
        private $item;

        /**
        * Order of element
        *
        * @var [int] $number
        */
        private $number;

        /**
         * Id of element
         *
         * @var [int]
         */
        private $id;

        /**
        * States of readiness
        *
        * NOTE: 0 -> undone
        *       1 -> done
        *
        * @var [int] $status
        */
        private $status;

        public function __construct($item, $number, $status, $starred, $id) {
            $this->item = $item;
            $this->number = $number;
            $this->status = $status;
            $this->starred = $starred;
            $this->id = $id;
        }

        public function get_item() {return $this->item;}
        public function set_item($item) {$this->item = $item;}

        public function get_id() {return $this->id;}
        public function set_id($id) {$this->id = $id;}

        public function get_number() {return $this->number;}
        public function set_number($number) {$this->number = $number;}

        public function get_status() {return $this->status;}
        // Sets status of instance to done (1)
        public function set_done() {$this->status = 1;}
        // Sets status of instance to undone (0)
        public function set_undone() {$this->status = 0;}

        /**
         * Returns if list item is done
         *
         * @return boolean
         */
        public function is_done() {return ($this->status == 1) ? true : false;}

        /**
         * Returns if list item is starred
         *
         * @return boolean
         */
        public function is_starred() {return ($this->starred == 1) ? true : false;}
    }