<?php

    class OneList {

        /**
        * Name of list
        *
        * @var [string] $name
        */
        private $name;

        /**
        * Contains list_item objects
        *
        * @var [array] $container
        */
        private $container;

        /**
         * Contains if list is starred
         * 1 - starred
         * 0 - unstarred
         *
         * @var [int]
         */
        private $star;

        public function __construct($name, $container, $star) {
            $this->name = $name;
            $this->container = $container;
            $this->star = $star;
        }

        public function get_name() {return $this->name;}
        public function set_name($name) {$this->name = $name;}

        public function get_container() {return $this->container;}
        public function set_container($container) {$this->container = $container;}

        public function is_starred() {return ($this->star == 1) ? true : false;}
        public function star() {$this->star = 1;}
        public function unstar() {$this->star = 0;}
    }