<?php

    class Purchase {
        
        /**
         * Content of purchase
         *
         * @var [string]
         */
        private $content;

        /**
         * Price of purchase
         *
         * @var [int]
         */
        private $price;

        public function __construct($content, $price) {
            $this->content = $content;
            $this->price = $price;
        }

        public function get_content() {return $this->content;}
        public function set_content($content) {$this->content = $content;}

        public function get_price() {return $this->price;}
        public function set_price($price) {$this->price = $price;}
    }