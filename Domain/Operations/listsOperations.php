<?php

    require_once(__DIR__."\..\..\initialization.php");

    /**
     * All things connected to lists
     * - adding new list
     * - deleting selected list
     * - 
     */
    class listsOperations {

        private $list;

        private $username;

        public function __construct($list)
        {
            $this->list = $list;
            $this->username = $_SESSION["username"];
        }

        /**
         * Adding new list to database
         *
         * @return void
         */
        public function add() {
            global $connection;

            // Blank listnames not tolerated
            if($this->list=="") {
                $_SESSION["error_msg"] = "No blank input!";
                return false;
            }

            // No duplicated listname allowed
            if($this->find($this->list)) {
                $_SESSION["error_msg"] = "List name already taken!";
                return false;
            }

            // Adding new list to database
            mysqli_query($connection, "INSERT INTO lists VALUES (
            '$this->list', '$this->username', '', 0, 0, 0, NOW(), '')") 
            or die($connection);

            // Biggest id
            $max = mysqli_query($connection, "SELECT MAX(id) as max FROM list_order
            WHERE user='$this->username'") or die($connection);

            $max = mysqli_fetch_assoc($max)['max'] + 1;

            // Setting listorder
            mysqli_query($connection, "INSERT INTO list_order VALUES (
            '$this->list', '$this->username', '$max')") or die($connection);
        }

        /**
         * Deleting selected list
         *
         * @return void
         */
        public function delete() {
            global $connection;

            $id = mysqli_query($connection, "SELECT id FROM list_order WHERE list_name='$this->list' 
            AND user='$this->username'") or die($connection);
            $id = mysqli_fetch_assoc($id)['id'];

            // Deletes list from lists table
            mysqli_query($connection, "DELETE FROM lists WHERE 
            username='$this->username' AND list_name='$this->list'") or 
            die($connection);

            // Deletes list from starred lists table
            mysqli_query($connection, "DELETE FROM starred_lists WHERE 
            user='$this->username' AND list_name='$this->list'") or 
            die($connection);

            // Deletes list from ordered lists table
            mysqli_query($connection, "DELETE FROM list_order WHERE 
            user='$this->username' AND list_name='$this->list'") or 
            die($connection);

            // Lowering higher lists
            mysqli_query($connection, "UPDATE list_order 
            SET id= id-1 WHERE user='$this->username' AND id>'$id'") or 
            die($connection);
        }

        /**
         * Modifies listname
         *
         * @param [string] $new
         * @return void
         */
        public function modify($new) {

            // Blank listnames not allowed
            if($new=="") {
                $_SESSION["error_msg"] = "No blank input!";
                return false;
            }

            // Duplicated listnames not allowed
            if($this->find($new)) {
                $_SESSION["error_msg"] = "List name already in db.";
                $_SESSION["merge_conflict_new"] = $new;
                $_SESSION["merge_conflict_list"] = $this->list;
            } 
            
            // Modifies listnames
            else {
                global $connection;

                // Modifies listname in lists
                mysqli_query($connection, "UPDATE lists SET list_name='$new' 
                WHERE list_name='$this->list' AND username='$this->username'")
                or die($connection);

                // Modifies listname in starred_lists
                mysqli_query($connection, "UPDATE starred_lists SET list_name='$new' 
                WHERE list_name='$this->list' AND user='$this->username'") 
                or die($connection);

                // Modifies listname in list_order
                mysqli_query($connection, "UPDATE list_order SET list_name='$new' 
                WHERE list_name='$this->list' AND user='$this->username'") 
                or die($connection);
            }
        }

        /**
         * Merges two lists
         *
         * @param [type] $new
         * @return void
         */
        public function merge($new) {
            global $connection;

            $id = mysqli_query($connection, "SELECT id FROM list_order 
            WHERE list_name='$this->list' AND user='$this->username'") or die($connection);
            $id = mysqli_fetch_assoc($id)['id'];

            // Modifies listname in lists
            mysqli_query($connection, "UPDATE lists SET list_name='$new' 
            WHERE list_name='$this->list' AND username='$this->username'")
            or die($connection);

            // Modifies listname in starredlists table
            mysqli_query($connection, "UPDATE starred_lists SET list_name='$new' 
            WHERE list_name='$this->list' AND user='$this->username'") 
            or die($connection);

            // Modifies listname in listorder table
            mysqli_query($connection, "UPDATE list_order SET list_name='$new' 
            WHERE list_name='$this->list' AND user='$this->username'") 
            or die($connection);

            // List that causes merge conflict
            $conflictlist = $_SESSION["merge_conflict_new"];

            // Deletes second list
            mysqli_query($connection, "DELETE FROM list_order 
            WHERE list_name='$conflictlist' AND user='$this->username' AND id='$id'") 
            or die($connection);

            // Lowers orders of higher lists
            mysqli_query($connection, "UPDATE list_order SET id= id-1 
            WHERE user='$this->username' AND id>'$id'") 
            or die($connection);

            // Merge conflict resolved
            $_SESSION["error_msg"] = null;
            $_SESSION["merge_conflict_list"] = null;
            $_SESSION["merge_conflict_new"] = null;
        }

        /**
         * Returns if listname is in database
         *
         * @param [string] $element
         * @return bool
         */
        public function find($element) {
            global $connection;

            $content = mysqli_query($connection, "SELECT list_name  
            FROM lists WHERE list_name='$element' AND username='$this->username'") 
            or die($connection);

            return mysqli_num_rows($content)!=0 ? true : false;
        }

        /**
         * Stars/unstars selected list
         *
         * @return void
         */
        public function star() {
            global $connection;

            // Stared state of a list
            $star = mysqli_query($connection, "SELECT * FROM starred_lists
            WHERE user='$this->username' AND list_name='$this->list'")
            or die($connection);

            // Unstars if stared
            if(mysqli_num_rows($star)) {
                mysqli_query($connection, "DELETE FROM starred_lists 
                WHERE list_name='$this->list' AND user='$this->username'") 
                or die($connection);
            } 

            // Stars if unstared
            else {
                mysqli_query($connection, "INSERT INTO starred_lists 
                VALUES ('$this->list', '$this->username')") 
                or die($connection);
            }
        }

        /**
         * Lowering selected item
         *
         * @return void
         */
        public function down() {
            global $connection;

            // Getting id of selected item
            $id = mysqli_query($connection, "SELECT id FROM list_order
            WHERE user='$this->username' AND list_name='$this->list'") 
            or die($connection);

            $id = mysqli_fetch_assoc($id)['id'];

            // Operation is interrupted if selected item is the last one
            if($id == $this->max()) return false;

            // Changing selected item's and following item's numbers

            // Moving a bit forward
            $set = $id+0.5;

            // Setting new id
            mysqli_query($connection, "UPDATE list_order SET id='$set' 
            WHERE user='$this->username' AND id='$id'") 
            or die($connection);

            // The one to be moved back
            $get = $id+1;

            // Setting id of following item
            mysqli_query($connection, "UPDATE list_order SET id='$id' 
            WHERE user='$this->username' AND id='$get'") 
            or die($connection);

            // Moving to the next position
            $set = $id+1;

            // The one that was moved forward a bit
            $get = $id+0.5;

            // Setting id to following item
            mysqli_query($connection, "UPDATE list_order SET id='$set' 
            WHERE user='$this->username' AND id='$get'")
            or die($connection);
        }

        /**
         * Raising selected item
         *
         * @return void
         */
        public function up() {
            global $connection;

            // Getting id of selected item
            $id = mysqli_query($connection, "SELECT id FROM list_order
            WHERE user='$this->username' AND list_name='$this->list'") 
            or die($connection);

            $id = mysqli_fetch_assoc($id)['id'];

            // Operation is interrupted if selected item is the first one
            if($id == 1) return false;

            // Changing selected item's and previous item's numbers

            // Moving a bit backward
            $set = $id-0.5;

            // Setting new id
            mysqli_query($connection, "UPDATE list_order SET id='$set' 
            WHERE user='$this->username' AND id='$id'")
            or die($connection);

            // The one to be moved forward
            $get = $id-1;

            // Setting id of previous item
            mysqli_query($connection, "UPDATE list_order SET id='$id' 
            WHERE user='$this->username' AND id='$get'")
            or die($connection);

            // Moving to the previous position
            $set = $id-1;

            // The one that was moved backward a bit
            $get = $id-0.5;

            // Setting id to previous item
            mysqli_query($connection, "UPDATE list_order SET id='$set' 
            WHERE user='$this->username' AND id='$get'")
            or die($connection);    
        }

        /**
         * Returns greates number of items in list 
         *
         * @return [int]
         */
        public function max() {
            global $connection;

            // Greatest number of items in list
            $max = mysqli_query($connection, "SELECT MAX(id) AS max
            FROM list_order WHERE user='$this->username'")
            or die($connection);

            return mysqli_fetch_assoc($max)['max'];
        }
    }