<?php

    require_once(__DIR__."\..\..\initialization.php");

    /**
     * All things connected to balance
     * - adding new category
     * - deleting selected category
     */
    class balanceOperations {

        // Category 
        private $name;
        // Logged in user
        private $user;

        public function __construct($name) {
            $this->name = $name;
            $this->user = $_SESSION["username"];
        }

        /**
         * Adding new category to database
         *
         * @return void
         */
        public function add() {
            global $connection;

            // If given category was found in the database
            if($this->find()) {
                $_SESSION["error_msg"] = "Category already added!";

                // Operation is aborted
                return false;
            }

            // Appending new category to database
            mysqli_query($connection, "INSERT INTO balance
            VALUES ('$this->user', '$this->name')") or die($connection);
        }

        /**
         * Deleting given category from the database
         *
         * @return void
         */
        public function delete() {
            global $connection;

            // Deleting category
            mysqli_query($connection, "DELETE FROM balance
            WHERE category='$this->name' AND user='$this->user'") or die($connection);

            // Deleting items belonging to deleted category
            mysqli_query($connection, "DELETE FROM balance_sheet
            WHERE category='$this->name' AND user='$this->user'") or die($connection);
        }

        /**
         * Returns whether category is in the database
         *
         * @return bool
         */
        public function find() {
            global $connection;

            $content = mysqli_query($connection, "SELECT user FROM balance
            WHERE category='$this->name' AND user='$this->user'") or die($connection);

            return mysqli_num_rows($content) ? true : false;
        }
    }