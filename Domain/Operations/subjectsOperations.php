<?php

    require_once(__DIR__."/../../initialization.php");
    
    /**
     * All things connected to subjects
     * - adding new subject
     * - deleting given subject
     * - adding new note
     * - deleting given note
     */
    class subjectsOperations {

        /**
         * All operations are linked to a subject
         *
         * @var [string]
         */
        private $subject;

        public function __construct($subject)
        {
            $this->subject = $subject;
        }
        
        /**
         * Adds new subject to database
         *
         * @return void
         */
        public function new_subject() {
            global $connection;

            $user = $_SESSION["username"];

            // If subjet already in database
            $already = mysqli_query($connection, "SELECT * 
            FROM notes WHERE user='$user' 
            AND subject='$this->subject'") or die($connection);

            if(mysqli_num_rows($already)) {
                $_SESSION["error_msg"] = "Already added!";
                go("/subject.php");

                // Operations aborted
                return false;
            }

            // Adding new subject to database
            mysqli_query($connection, "INSERT INTO notes
            (user, subject, note) VALUES ('$user', '$this->subject', '')") 
            or die($connection);
        }

        /**
         * Deleting selected subject from database
         *
         * @return void
         */
        public function delete_subject() {
            global $connection;

            $user = $_SESSION["username"];

            // Deleting selected subject from database
            mysqli_query($connection, "DELETE FROM notes
            WHERE user='$user' AND subject='$this->subject'") 
            or die($connection);
        }

        /**
         * Adding new note to database
         *
         * @param [int] $note
         * @return void
         */
        public function add_note($note) {
            global $connection;

            $user = $_SESSION["username"];

            // Adding new note to database
            mysqli_query($connection, "INSERT INTO notes 
            VALUES ('$user', '$this->subject', '$note')") or die($connection);
        }

        /**
         * Deleting given note from the database
         *
         * @param [int] $note
         * @return void
         */
        public function delete_note($note) {
            global $connection;

            $user = $_SESSION["username"];

            // Deleting given note from the database
            mysqli_query($connection, "DELETE FROM notes
            WHERE user='$user' AND subject='$this->subject' AND note='$note' LIMIT 1") 
            or die($connection);
        }
    }