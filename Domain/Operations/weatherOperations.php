<?php

    require_once(__DIR__."/../../initialization.php");
    
    /**
     * All things connected to weather
     * - adding new place
     * - deleting given place
     */
    class weatherOperations {

        /**
         * Operations are linked to a place
         *
         * @var [string]
         */
        private $place;

        public function __construct($place)
        {
            $this->place = $place;
        }
        
        /**
         * Adds new place to the database
         *
         * @return int // error code
         */
        public function new_place() {
            global $connection;

            $user = $_SESSION["username"];

            // OpenWeather does not allow spaces
            $this->place = str_replace(" ","-", $this->place);

            // If data is already in database
            $already = mysqli_query($connection, "SELECT * 
            FROM weather WHERE user='$user' 
            AND place='$this->place'") or die($connection);

            // If place not found on OpenWeather
            if(!$this->check_place()) return 0; 

            // If already in database
            else if(mysqli_num_rows($already) != '') return 1;

            else {

                // Adding new place to database
                mysqli_query($connection, "INSERT INTO weather
                (user, place) VALUES ('$user', '$this->place')") 
                or die($connection);

                return 2;
            }
           
        }

        /**
         * Delets given place from the database
         *
         * @return void
         */
        public function delete_place() {
            global $connection;

            $user = $_SESSION["username"];

            // Deleting given record
            mysqli_query($connection, "DELETE FROM weather
            WHERE user='$user' AND place='$this->place'") 
            or die($connection);
        }

        /**
         * Cheks if place is on OpenWeather
         *
         * @return bool
         */
        public function check_place() {
            global $config;

            // OpenWeather works with appid
            $weatherKey = $config['weather']['key'];

            // Getting weather information
            $weather = file_get_contents('http://api.openweathermap.org/data/2.5/weather?q='.$this->place.'&appid='.$weatherKey);

            // Successful case -> string contains 'weather' 
            return strpos($weather, 'weather') ? true : false;
        }

        /**
         * Getting weather information
         *
         * @return Weather
         */
        public function get_data() {
            global $config;

            // OpenWeather works with appid
            $weatherKey = $config['weather']['key'];

            // Getting weather information
            $weather = file_get_contents('http://api.openweathermap.org/data/2.5/weather?q='.$this->place .'&appid='.$weatherKey);
            $json = json_decode($weather);

            $temperature = $json->{'main'}->{'temp'};
            $celsius =  round($temperature-273.15);
            $description = $json->{'weather'};
            $description = $description['0']->{'main'};

            return new Weather($this->place, $celsius, $description);
        }
    }