<?php

    require_once(__DIR__."/../../initialization.php");
    
    /**
     * All things connected to authentication
     * - logging in
     * - registering
     * - logging out
     */
    class authenticationOperations {

        /**
         * authentication is linked to a user
         *
         * @var [user]
         */
        private $user;

        /**
         * Loads in the user
         *
         * @param [user] $user
         */
        public function __construct($user)
        {
            $this->user = $user;
        }
        
        /**
         * Logs in user
         *
         * @return void
         */
        public function login() {

            // Logged in user has access to all pages
            $_SESSION["access"] = true;

            // Logged in user is now given username
            $_SESSION["username"] = $this->user->get_username();
        }

        /**
         * Inserts new user's data in database
         *
         * @return void
         */
        public function register() {

            $username = $this->user->get_username();
            $password = $this->user->get_password(); 

            global $connection;

            // Appends new data in database
            mysqli_query($connection, "INSERT INTO authentication (username, password) 
            VALUES ('$username', '$password')") 
            or die(mysqli_error($connection));

            // After data is stored, new user is automatically logged in
            $this->login();
        }

        /**
         * Logs out user
         *
         * @return void
         */
        public function logout() {

            // No user is logged in
            $_SESSION["username"] = null;

            // Access is denied to (most) pages, if no user is logged in 
            $_SESSION["access"] = false;

            session_destroy();
        }
    }