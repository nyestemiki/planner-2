<?php

    require_once(__DIR__."\..\..\initialization.php");

    /**
     * All things connected to lists
     *      (operation with a certain list)
     * - adding new list item
     * - deleting selected item
     * - starring item
     * - done/ undone item 
     * - changing position
     *      - up
     *      - down
     */
    class listOperations {

        /**
         * List item
         *
         * @var [List_item]
         */
        private $element;

        /**
         * List the element belongs to
         *
         * @var [string]
         */
        private $list;

        /**
         * User the element is created by
         *
         * @var [string]
         */
        private $username;

        public function __construct($list, $element) {
            $this->list = $list;
            $this->element = $element;
            $this->username = $_SESSION["username"];
        }

        /**
         * Adds new list item to certain list
         *
         * @return void
         */
        public function add() {
            global $connection;

            // Cannot add empty record
            if($this->element=="") return false;

            // Max number of list items plus one is new list items number
            $max_plus = $this->max()+1;

            // Appending new list item
            mysqli_query($connection, "INSERT INTO lists 
            VALUES ('$this->list', '$this->username', '$this->element', 
            '$max_plus', 0, 0, NOW(), '')") 
            or die($connection);
        }

        /**
         * Deletes given list item form certain list
         *
         * @return void
         */
        public function delete() {
            global $connection;

            // Getting number of the list item to be deleted
            $number = mysqli_query($connection, "SELECT number FROM lists
            WHERE username='$this->username' AND list_name='$this->list'
            AND item_id='$this->element'") or die($connection);

            $number = mysqli_fetch_assoc($number)['number'];

            // Deletes given list item form given list
            mysqli_query($connection, "DELETE FROM lists 
            WHERE username='$this->username' AND item_id='$this->element' 
            AND list_name='$this->list'") 
            or die($connection);

            // Modifies number of list items greater than delete item's number
            mysqli_query($connection, "UPDATE lists 
            SET number= number-1
            WHERE username='$this->username' AND number>'$number' 
            AND list_name='$this->list'") 
            or die($connection);
        }

        /**
         * Returns whether list item is starred
         *
         * @return boolean
         */
        public function isstarred() {
            global $connection;

            // Getting star attribute of selected list item
            $starred = mysqli_query($connection, "SELECT starred FROM lists 
            WHERE username='$this->username' AND list_name='$this->list' 
            AND item_id='$this->element' AND starred=1") 
            or die ($connection);

            return mysqli_num_rows($starred) ? true : false;
        }

        /**
         * Stars/unstars selected list item
         *
         * @return void
         */
        public function star() {
            global $connection;

            // Unstars selected item if it was stared
            if($this->isstarred()) {
                mysqli_query($connection, "UPDATE lists SET starred=0 
                WHERE username='$this->username' AND list_name='$this->list' 
                AND item_id='$this->element' AND starred=1") 
                or die($connection);
            } 
            
            // Stars selected list item if it was unstared
            else {
                mysqli_query($connection, "UPDATE lists SET starred=1 
                WHERE username='$this->username' AND list_name='$this->list' 
                AND item_id='$this->element' AND starred=0") 
                or die($connection);
            }
        }

        /**
         * Returns whether selected list item is done
         *
         * @return boolean
         */
        public function isdone() {
            global $connection;

            // Getting done attribute of selected list item
            $done = mysqli_query($connection, "SELECT status FROM lists 
            WHERE username='$this->username' AND list_name='$this->list' 
            AND item_id='$this->element' AND status=1") or die ($connection);

            return mysqli_num_rows($done) ? true : false; 
        }

        /**
         * Sets done/undone selected list item
         *
         * @return void
         */
        public function state() {
            global $connection;

            // Sets selected item done if it was undone
            if($this->isdone()) {
                mysqli_query($connection, "UPDATE lists SET status=0 
                WHERE username='$this->username' AND list_name='$this->list' 
                AND item_id='$this->element' AND status=1") 
                or die($connection);
            } 
            
            // Sets selected item undone if it was done
            else {
                mysqli_query($connection, "UPDATE lists SET status=1 
                WHERE username='$this->username' AND list_name='$this->list' 
                AND item_id='$this->element' AND status=0") 
                or die($connection);
            }
        }

        /**
         * Lowering selected item
         *
         * @return void
         */
        public function down() {
            global $connection;

            // Getting number of selected item
            $number = mysqli_query($connection, "SELECT number FROM lists
            WHERE username='$this->username' AND list_name='$this->list'
            AND item_id='$this->element'") 
            or die($connection);

            $number = mysqli_fetch_assoc($number)['number'];

            // Operation is interrupted if selected item is the last one
            if($number == $this->max()) return false;

            // Changing selected item's and following item's numbers

            // Moving a bit forward
            $set = $number+0.5;

            // Setting new number
            mysqli_query($connection, "UPDATE lists SET number='$set' 
            WHERE list_name='$this->list' AND username='$this->username' AND number='$number'") 
            or die($connection);

            // The one to be moved back
            $get = $number+1;

            // Setting number of following item
            mysqli_query($connection, "UPDATE lists SET number='$number' 
            WHERE list_name='$this->list' AND username='$this->username' AND number='$get'") 
            or die($connection);

            // Moving to the next position
            $set = $number+1;

            // The one that was moved forward a bit
            $get = $number+0.5;

            // Setting number to following item
            mysqli_query($connection, "UPDATE lists SET number='$set' 
            WHERE list_name='$this->list' AND username='$this->username' AND number='$get'")
            or die($connection);
        }

        /**
         * Raising selected item
         *
         * @return void
         */
        public function up() {
            global $connection;

            // Getting number of selected item
            $number = mysqli_query($connection, "SELECT number FROM lists
            WHERE username='$this->username' AND list_name='$this->list'
            AND item_id='$this->element'") 
            or die($connection);

            $number = mysqli_fetch_assoc($number)['number'];

            // Operation is interrupted if selected item is the first one
            if($number == 1) return false;

            // Changing selected item's and previous item's numbers

            // Moving a bit backward
            $set = $number-0.5;

            // Setting new number
            mysqli_query($connection, "UPDATE lists SET number='$set' 
            WHERE list_name='$this->list' AND username='$this->username' AND number='$number'")
            or die($connection);

            // The one to be moved forward
            $get = $number-1;

            // Setting number of previous item
            mysqli_query($connection, "UPDATE lists SET number='$number' 
            WHERE list_name='$this->list' AND username='$this->username' AND number='$get'")
            or die($connection);

            // Moving to the previous position
            $set = $number-1;

            // The one that was moved backward a bit
            $get = $number-0.5;

            // Setting number to previous item
            mysqli_query($connection, "UPDATE lists SET number='$set' 
            WHERE list_name='$this->list' AND username='$this->username' AND number='$get'")
            or die($connection);    
        }

        /**
         * Returns greates number of items in list 
         *
         * @return [int]
         */
        public function max() {
            global $connection;

            // Greatest number of items in list
            $max = mysqli_query($connection, "SELECT MAX(number) AS max
            FROM lists WHERE username='$this->username' AND list_name='$this->list'")
            or die($connection);

            return mysqli_fetch_assoc($max)['max'];
        }

        /**
         * Returns if element is found in database
         *
         * @param [string] $element
         * @return bool
         */
        public function find($element) {
            global $connection;

            // Getting item by id
            $content = mysqli_query($connection, "SELECT * FROM lists 
            WHERE list_name='$this->list' AND username='$this->username'
            AND item_id='$element'") 
            or die($connection);

            return mysqli_num_rows($content) ? true : false; 
        }
    }