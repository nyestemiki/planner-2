<?php

    require_once(__DIR__."/../../initialization.php");

    /**
     * All things connected to balance 
     *      (operations with a certain category)
     * - adding new purchase
     * - deleting given purchase
     */
    class balanceSheetOperations {

        // purchase's name
        private $content;
        // purchase's price
        private $price;
        // category the purchase belongs to
        private $category;
        //user the purchase belongs to
        private $user;

        public function __construct($content, $price, $category) {
            $this->content = $content;
            $this->price = $price;
            $this->category = $category;
            $this->user = $_SESSION["username"];
        }

        /**
         * Adding new purchase to the database
         *
         * @return void
         */
        public function add() {
            global $connection;

            // If purchase already in database, adds new price to the record
            if($this->find()) {
                mysqli_query($connection, "UPDATE balance_sheet
                SET price=price+'$this->price'
                WHERE user='$this->user' AND category='$this->category' AND content='$this->content'") 
                or die($connection);
                $_SESSION["error_msg"] = "Merged";
            } 
            
            // If purchase not in database, adds it
            else {
                mysqli_query($connection, "INSERT INTO balance_sheet
                VALUES ('$this->user', '$this->category', '$this->content', '$this->price')") or die($connection);
            }
        }

        /**
         * Deleting given purchase
         *
         * @return void
         */
        public function delete() {
            global $connection;

            mysqli_query($connection, "DELETE FROM balance_sheet
            WHERE category='$this->category' AND user='$this->user' AND
            content='$this->content' AND price='$this->price'") or die($connection);
        }

        /**
         * Returns whether purchase is in the database
         *
         * @return bool
         */
        public function find() {
            global $connection;

            $content = mysqli_query($connection, "SELECT user FROM balance_sheet
            WHERE category='$this->category' AND user='$this->user' AND content='$this->content'") or die($connection);

            return mysqli_num_rows($content) ? true : false;
        }
    }