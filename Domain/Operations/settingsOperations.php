<?php

    require_once(__DIR__."\..\..\initialization.php");

    /**
     * All things connected to settings
     * - change username
     * - change password
     * - delete user
     */
    class settingsOperations {

        /**
         * Changes username
         *
         * @param [string] $new
         * @return void
         */
        public function change_username($new) {

            // If username is already in database
            if($this->find($new)) {
                $_SESSION["error_msg"] = "Username taken!";
                go("/UI/settings.php");

                // Operation is aborted
                return false;
            }

            global $connection;

            $name = $_SESSION['username'];

            // Changes logged in user
            $_SESSION["username"] = $new;

            // Changes username in list of users
            mysqli_query($connection, "UPDATE authentication SET username='$new' WHERE username='$name'") or die($connection);
            // Changes username in balance
            mysqli_query($connection, "UPDATE balance SET user='$new' WHERE user='$name'") or die($connection);
            // Changes username in balance_sheet
            mysqli_query($connection, "UPDATE balance_sheet SET user='$new' WHERE user='$name'") or die($connection);
            // Changes username in lists
            mysqli_query($connection, "UPDATE lists SET username='$new' WHERE username='$name'") or die($connection);
            // Changes username in list_order
            mysqli_query($connection, "UPDATE list_order SET user='$new' WHERE user='$name'") or die($connection);
            // Changes username in notes
            mysqli_query($connection, "UPDATE notes SET user='$new' WHERE user='$name'") or die($connection);
            // Changes username in starred_lists
            mysqli_query($connection, "UPDATE starred_lists SET user='$new' WHERE user='$name'") or die($connection);
            // Changes username in timetable
            mysqli_query($connection, "UPDATE timetable SET username='$new' WHERE username='$name'") or die($connection);
            // Changes username in weather
            mysqli_query($connection, "UPDATE weather SET user='$new' WHERE user='$name'") or die($connection);
        }

        /**
         * Returns of username is in database
         *
         * @param [string] $name
         * @return bool
         */
        public function find($name) {
            global $connection;

            // Getting appearances of user in database
            $request = mysqli_query($connection, "SELECT username FROM authentication 
            WHERE username='$name'") or die($connection);
            
            // If query returned any record, user was found in database
            return (mysqli_num_rows($request)) ? true : false;
        }

        /**
         * Changes user's password
         *
         * @param [string] $new
         * @return void
         */
        public function change_password($new) {

            global $connection;

            $name = $_SESSION['username'];

            // Passwords are safe with sha1 encoding
            $safepass = sha1($new);

            // Changes user's password
            mysqli_query($connection, "UPDATE authentication SET password='$safepass' 
            WHERE username='$name'") or die($connection);
        }

        /**
         * Deletes user and its data
         *
         * @return void
         */
        public function delete() {
            global $connection;

            $name = $_SESSION['username'];

            // Deletes username in list of users
            mysqli_query($connection, "DELETE FROM authentication WHERE username='$name'") or die($connection);
            // Deletes username in balance
            mysqli_query($connection, "DELETE FROM balance WHERE user='$name'") or die($connection);
            // Deletes username in balance_sheet
            mysqli_query($connection, "DELETE FROM balance_sheet WHERE user='$name'") or die($connection);
            // Deletes username in lists
            mysqli_query($connection, "DELETE FROM lists WHERE username='$name'") or die($connection);
            // Deletes username in list_order
            mysqli_query($connection, "DELETE FROM list_order WHERE user='$name'") or die($connection);
            // Deletes username in notes
            mysqli_query($connection, "DELETE FROM notes WHERE user='$name'") or die($connection);
            // Deletes username in starred_lists
            mysqli_query($connection, "DELETE FROM starred_lists WHERE user='$name'") or die($connection);
            // Deletes username in timetable
            mysqli_query($connection, "DELETE FROM timetable WHERE username='$name'") or die($connection);
            // Deletes username in weather
            mysqli_query($connection, "DELETE FROM weather WHERE user='$name'") or die($connection);

            // Access denied to most pages
            $_SESSION["access"] = false;

            // Username is deleted
            $_SESSION["username"] = null;
        }
    }