<?php

    require_once(__DIR__."/../initialization.php");
    
    /**
     * A user has a unique username and a password
     */
    class User {

        /**
         * User's name
         *
         * @var [string]
         */
        private $username;

        /**
         * User's hashed password
         *
         * @var [string]
         */
        private $password;

        public function __construct($username, $password) {
            $this->username = $username;

            // Password is safe by sha1 encoding
            $this->password = sha1($password);
        }

        public function get_username() {return $this->username;}
        public function set_username($username) {$this->username = $username;}

        public function get_password() {return $this->password;}
        public function set_password($password) {$this->password = $password;}
    }