<?php

    require_once(__DIR__."/../initialization.php");
    
    class Weather {

        /**
         * Place
         *
         * @var [string]
         */
        private $place;

        /**
         * Temperature 
         *      (in celsius)
         *
         * @var [float]
         */
        private $degree;

        /**
         * Small description of weather at selected place
         *
         * @var [string]
         */
        private $description;

        public function __construct($place, $degree, $description) {
            $this->place = $place;
            $this->degree = $degree;
            $this->description = $description;
        }

        public function get_place() {return $this->place;}
        public function set_place($place) {$this->place = $place;}

        public function get_degree() { return $this->degree;}
        public function set_degree($degree) {$this->degree = $degree;}

        public function get_description() {return $this->description;}
        public function set_description($description) {$this->description = $description;}
    }