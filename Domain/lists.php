<?php

    class Lists {

        /**
        * Contains list objects
        *
        * @var [array] $container
        */
        private $container;

        public function __construct($container) {
            $this->container = $container;
        }

        public function get_container() {return $this->container;}
        public function set_container($container) {$this->container = $container;}
    }