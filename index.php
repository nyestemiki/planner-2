<?php 

    require_once("initialization.php");

    // Access granted for all users
    $GUEST_ACCESS = true;

    // If user is logged in, index points at menu page
    if(isset($_SESSION["username"]))
        go("UI/menu.php");

    // If user is not logged in, index points at authentication page
    else go("UI/authentication.php");   