<?php

    require_once(__DIR__."/../initialization.php");

    /**
     * Loads places
     *
     * @return void
     */
    function load_weather() {
        global $connection;

        $user = $_SESSION['username'];

        // Getting all of the user's saved places
        $content = mysqli_query($connection, "SELECT * FROM weather
        WHERE user='$user'") or die($connection);

        $array = array();

        for($i = 0; $i < mysqli_num_rows($content); $i++) {
            $result = mysqli_fetch_assoc($content);

            $a = new weatherOperations($result["place"]);

            // Storing the weather data
            $array[] = $a->get_data();
        }

        return $array;
    }