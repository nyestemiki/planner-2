<?php 

    require_once(__DIR__."/../initialization.php");

    /**
     * Returns array of notes for selected subject
     *
     * @param [string] $subject
     * @return void
     */
    function load_notes($subject) {
        global $connection;

        $user = $_SESSION['username'];

        // Getting all of the user's notes for selected subject
        $content = mysqli_query($connection, "SELECT note FROM notes
        WHERE user='$user' AND subject='$subject'") or die($connection);

        $array = array();

        for($i = 0; $i < mysqli_num_rows($content); $i++) {
            $result = mysqli_fetch_assoc($content)['note'];

            // Storring the note
            $array[] = $result;
        }

        return $array;
    }