<?php

    require_once(__DIR__."/../initialization.php");
    
    /**
     * Returns array with a certain list by date
     *
     * @param [string] $selectedList
     * @return array
     */
    function get_list_date($selectedList) {
        global $connection;
        
        $username = $_SESSION['username'];

        // Getting list items for selected list
        $content =  mysqli_query($connection, "SELECT * FROM lists
        WHERE list_name='$selectedList' AND username='$username' AND
        item!='' ORDER BY date ASC") or die($connection);

        return load_list($content);
    }