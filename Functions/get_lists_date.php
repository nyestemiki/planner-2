<?php

    require_once(__DIR__."/../initialization.php");

    /**
     * Returns array of lists ordered by date
     *
     * @return array
     */
    function get_lists_date() {
        $username = $_SESSION["username"];

        global $connection;

        // Getting all of the user's lists
        $content = mysqli_query($connection, "SELECT list_name FROM lists 
        WHERE username='$username' ORDER BY date")
        or die($connection);

        // Will contain names of user's lists as array
        $list_array = array();

        // Loading list names in array distinct
        for ($i = 0; $i < mysqli_num_rows($content); $i++) {
            $result = mysqli_fetch_assoc($content);
            
            if (!in_array($result["list_name"], $list_array)) {
                $list_array[] = $result["list_name"];
            }
        }

        return load_lists($list_array);
    }