<?php

    require_once(__DIR__."/../initialization.php");

    /**
     * Returns array of list_items
     *
     * @param [array] $content
     * @return array
     */
    function load_list($content) {
        $array = array();

        for($i = 0; $i < mysqli_num_rows($content); $i++) {
            $result = mysqli_fetch_assoc($content);
            $item = new List_item($result["item"], $result["number"], $result["status"], $result["starred"], $result['item_id']);
            $array[] = $item;
        }

        return $array;
    }