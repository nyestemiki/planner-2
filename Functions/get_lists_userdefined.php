<?php

    require_once(__DIR__."/../initialization.php");

    /**
     * Returns array of lists in an order by the user
     *
     * @return void
     */
    function get_lists_userdefined() {
        $username = $_SESSION["username"];

        global $connection;

        // Getting all of the user's lists
        $content = mysqli_query($connection, "SELECT list_name FROM list_order 
        WHERE user='$username' ORDER BY id ASC")
        or die($connection);

        // Will contain names of user's lists as array
        $list_array = array();

        // Loading list names in array distinct
        for ($i = 0; $i < mysqli_num_rows($content); $i++) {
            $result = mysqli_fetch_assoc($content);
            
            if (!in_array($result["list_name"], $list_array)) {
                $list_array[] = $result["list_name"];
            }
        }

        return load_lists($list_array);
    }