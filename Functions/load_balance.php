<?php 
    
    include_once(__DIR__."/../initialization.php");

    /**
     * Returns array of categories
     *
     * @return array
     */
    function load_balance() {
        global $connection;

        $user = $_SESSION["username"];

        // Getting all the user's categories
        $content = mysqli_query($connection, "SELECT category FROM balance
        WHERE user='$user'") or die($connection);

        $array = array();

        for($i=0; $i<mysqli_num_rows($content); $i++) {
            $result = mysqli_fetch_assoc($content)["category"];

            $array[] = $result;
        }

        return $array;
    }