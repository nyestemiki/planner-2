<?php

    require_once(__DIR__."/../initialization.php");
    
    /**
     * Returns array with a certain list ordered by the user
     *
     * @param [string] $selectedList
     * @return array
     */
    function get_list_userdefined($selectedList) {
        global $connection;
        
        $username = $_SESSION['username'];

        // Getting list items for selected list
        $content =  mysqli_query($connection, "SELECT * FROM lists
        WHERE list_name='$selectedList' AND username='$username' AND
        item!='' ORDER BY number ASC") or die($connection);

        return load_list($content);
    }