<?php

    require_once(__DIR__."/../initialization.php");

    /**
     * Loads lists with list items
     *
     * @param [type] $content
     * @return void
     */
    function load_lists($content) {
        $username = $_SESSION["username"];

        global $connection;
        
        $array = array();
        
        foreach($content as $a) {

            // Getting all of the user's lists and list items
            $content = mysqli_query($connection, "SELECT * FROM lists 
            WHERE username='$username' AND list_name='$a' ORDER BY number ASC") or die($connection);

            // Will contain items of a list (at a time)
            $items = array();

            // Creating list_item objects
            for($i = 0; $i < mysqli_num_rows($content); $i++) {
                $result = mysqli_fetch_assoc($content);

                $item = new List_item($result["item"], $result["number"], $result["status"], $result["starred"], $result["item_id"]);

                // Storing new list item
                $items[] = $item;
            }

            // If list is starred
            $star = mysqli_query($connection, "SELECT * FROM starred_lists 
            WHERE user='$username' AND list_name='$a'") or die($connection);

            // Adding star state to list
            $list = mysqli_num_rows($star) ? new OneList($a, $items, 1) : new OneList($a, $items, 0);

            // Saving list
            $array[] = $list;
        }

        // Making the lists object
        $lists = new Lists($array);

        return $lists;
    }