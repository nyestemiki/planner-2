<?php

    require_once(__DIR__."/../initialization.php");

    /**
     * Returns array of timetable events
     *
     * @return array
     */
    function load_timetable() {
        global $connection;

        $user = $_SESSION['username'];

        // Getting all of the user's timetable data
        $content = mysqli_query($connection, "SELECT * FROM timetable
        WHERE username='$user'") or die($connection);

        $array = array();

        for($i=0; $i < mysqli_num_rows($content); $i++) {
            $result = mysqli_fetch_assoc($content);

            // Storing timetable data
            $array[$result['id']] = $result['content'];
        }

        return $array;
    }