<?php 

    /**
     * Redirects from current page to $link
     *
     * @param [string] $link
     * @return void
     */
    function go($link){
        header('Location: '.$link);
    }