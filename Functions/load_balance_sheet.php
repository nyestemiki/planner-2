<?php

    require_once(__DIR__."/../initialization.php");

    /**
     * Returns array of purchases for selected category
     *
     * @param [string] $category
     * @return array
     */
    function load_balance_sheet($category) {
        global $connection;

        $user = $_SESSION["username"];

        // Getting all the user's purchases for a certain category
        $content = mysqli_query($connection, "SELECT content, price FROM balance_sheet
        WHERE user='$user' AND category='$category'") or die($connection);

        $array = array();

        for($i=0; $i<mysqli_num_rows($content); $i++) {
            $result = mysqli_fetch_assoc($content);

            $purchase = new Purchase($result['content'], $result['price']);

            $array[] = $purchase;
        }

        return $array;
    }