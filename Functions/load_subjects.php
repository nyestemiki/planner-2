<?php

    require_once(__DIR__."/../initialization.php");

    /**
     * Returns array of user's subjects
     *
     * @return void
     */
    function load_subjects() {
        global $connection;

        $user = $_SESSION['username'];

        // Getting all of the user's subjects
        $content = mysqli_query($connection, "SELECT DISTINCT subject FROM notes
        WHERE user='$user'") or die($connection);

        $array = array();

        for($i = 0; $i < mysqli_num_rows($content); $i++) {
            $result = mysqli_fetch_assoc($content)['subject'];

            // Storing the subject
            $array[] = $result;
        }

        return $array;
    }