<?php require_once("../initialization.php"); ?>

<!DOCTYPE HTML>
<html>
    <head>
        <title> Weather </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../main.css">
    </head>
    <body>
        <script>

            // Empty content of input area
            function empty() {
                document.getElementById("newplace").value = "";
            }

            // Getting and storing new place
            function new_weather() {
                place = document.getElementById('newplace').value;

                // Asyncronous operations
                xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {

                        // Place not found
                        if(this.responseText.indexOf('Place not found') != -1) {
                            document.getElementById("errormsg").classList.add("show");
                            document.getElementById("errormsg").innerHTML = "Place not found";
                            document.getElementById("newplace").value = "New place";
                        } 
                        
                        // Place already added
                        else if(this.responseText.indexOf('Already added') != -1) {
                            document.getElementById("errormsg").classList.add("show");
                            document.getElementById("errormsg").innerHTML = "Already added";
                            document.getElementById("newplace").value = "New place";
                        } 
                        
                        // No error occurred
                        else {
                            document.getElementById("menu").innerHTML += this.responseText;
                        }

                        setTimeout(function() {
                            document.getElementById("errormsg").classList.remove("show");
                        }, 2500);
                    }
                };

                // Storing new place and getting its data
                xmlhttp.open("GET", "../Ajax/new_weather.php?place="+place, true);
                xmlhttp.send();
            }

            // Deleting place
            function del(element) {

                // Asyncronous operation
                xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {

                        // Remove certain divs 
                        document.getElementById("widget//"+element.id).remove();
                    }
                };

                // Deleting place
                xmlhttp.open("GET", "../Controller/weather_controller.php?delete&place="+element.id, true);
                xmlhttp.send();
            }
            
        </script>

        <div class="wrapper">
            <!-- Add new place -->
            <div class="two_inline">
                <!-- New place input area -->
                <textarea onclick="empty()" class="textbox longbox" id="newplace">New place</textarea>
                <!-- Add place button -->
                <button onclick="new_weather()" class="btn">New</button>
            </div>

            <!-- Rendering the places -->
            <div class="menu" id="menu">
            <?php foreach(load_weather() as $place) : ?>
                <!-- Weather tile -->
                <div class="weather btn" id="widget//<?=$place->get_place()?>">
                    <!-- Place -->
                    <span class="place"><?=$place->get_place()?></span>
                    <!-- Temperature -->
                    <span class="degree"><?=$place->get_degree()?></span>
                    <!-- Description -->
                    <span class="description"><?=$place->get_description()?></span>
                    <!-- Delete button -->
                    <button onclick="del(this)" class="delete-weather" id="<?=$place->get_place()?>">Delete</button>
                </div>
            <?php endforeach ?>
            </div>

            <!-- Errors -->
            <span id="errormsg" class="errormsg errorcls hide"></span>
        </div>
    </body>
</html>