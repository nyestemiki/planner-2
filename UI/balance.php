<?php require_once("../initialization.php"); ?>

<!DOCTYPE HTML>
<html>
    <head>
        <title>Balance</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../main.css">
    </head>
    <body>
        <script>
            // Showing delete confirmation
            function del(element) {
                document.getElementById("del"+element.id).classList.add('show');
                document.getElementById("el"+element.id).classList.add('hide');
            }

            // Hiding delete confirmation
            function keep(element) {
                document.getElementById("del"+element.id).classList.remove('show');
                document.getElementById("el"+element.id).classList.remove('hide');
            }
        </script>

        <div class="wrapper <?php if(isset($_SESSION["error_msg"])) {
        echo "error";} ?>">

            <!-- Add category -->
            <form method="POST" action="../Controller/balance_controller.php">
                <input type="text" name="name" class="textbox" placeholder="New category" required>
                <input type="submit" name="add" hidden>
            </form>

            <?php $i=0 ?>

            <!-- Categories -->
            <div class="menu">
            <?php foreach(load_balance() as $category) : ?>
                <!-- Category -->
                <div class="square btn hover-show" id="el<?=$i?>">
                    <!-- Balance sheet -->
                    <a href="balance_sheet.php?category=<?=$category?>"><?=$category?></a>
                    <!-- Delete button -->
                    <button onclick="del(this)" id="<?=$i?>" class="hide btn delete nomargin">Delete</a>
                </div>

                <!-- Delete confirmation -->
                <div class="del two_inline hide" id="del<?=$i?>">
                    <!-- Category -->
                    <span class="btn nomargin"><?=$category?></span>
                    <!-- Confirm delete -->
                    <a href="../Controller/balance_controller.php?delete&name=<?=$category?>"
                    class="btn red">Delete</a>
                    <!-- Decline delete button -->
                    <button onclick="keep(this)" id="<?=$i++?>" class="btn">Keep</button>
                </div>
            <?php endforeach ?>
            </div>

            <!-- Errors -->
            <span class="errorcls errormsg">
            <?php if(isset($_SESSION["error_msg"])) {
                echo $_SESSION["error_msg"];

                // Deleting error messages
                $_SESSION["error_msg"] = null; 
            }?>
            </span>
        </div>
    </body>
</html>