<?php require_once("../initialization.php"); ?>

<!DOCTYPE HTML>
<html>
    <head>
        <title> Lists </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../main.css">
    </head>
    <body>
        <script>

            // Shows list name change window
            function insert(id) {
                document.getElementById("inputarea"+id.value).classList.add('show');
                document.getElementById("del"+id.value).classList.remove('show');
            }

            // Hides list name change window
            function decline(id) {
                document.getElementById("inputarea"+id.value).classList.remove('show');
            }

            // Shows list delete window
            function del(id) {
                document.getElementById("del"+id.value).classList.add('show');
                document.getElementById("inputarea"+id.value).classList.remove('show');
            }

            // Hides list delete window
            function keep(id) {
                document.getElementById("del"+id.value).classList.remove('show');
                document.getElementById("del"+id.value).classList.add('hide');
            }

            // Shows list order box
            function show() {
                document.getElementById("order-menu").classList.add('show');
                document.getElementById("selected-menu-order").classList.add('hide');
            }
        </script>

        <div class="wrapper">

            <!-- List order box button-->
            <button class="textbox" id="selected-menu-order" onclick="show()"> 
            <?php 
                if(isset($_REQUEST['mode']) && $_REQUEST['mode'] != "" && $_REQUEST['mode'] != "usdef")
                    echo $_REQUEST["mode"];
                else echo 'Userdefined';
            ?>
            </button>

            <!-- Order menu box (open) -->
            <div class="order-menu textbox" id="order-menu">
                <!-- Abc order -->
                <a href="?mode=abc" class="<?php if(isset($_REQUEST["mode"]) &&
                $_REQUEST["mode"]=='abc') echo 'blue'?>"> 
                    Abc 
                </a>

                <!-- Date order -->
                <a href="?mode=date" class="<?php if(isset($_REQUEST["mode"]) &&
                $_REQUEST["mode"]=="date") echo 'blue'?>"> 
                    Date 
                </a>

                <!-- Userdefined order -->
                <a href="?" class="<?php if(!isset($_REQUEST["mode"])) echo 'blue'?>"> 
                    Userdefined 
                </a>
            </div>

            <!-- Getting the mode -->
            <?php 
                if(isset($_REQUEST['mode']) && $_REQUEST['mode'] != "")
                    $mode=$_REQUEST['mode'];
                else $mode="usdef";
            ?>

            <!-- Getting the loaded list -->
            <?php 
                if(isset($_REQUEST["mode"]) && $_REQUEST["mode"]=='abc')
                    $lists = get_lists_abc();
                else if(isset($_REQUEST["mode"]) && $_REQUEST["mode"]=='date') 
                    $lists = get_lists_date();
                else
                    $lists = get_lists_userdefined();

                $lists = $lists->get_container();
            ?>

            <!-- Add new list -->
            <form method="POST" action="../Controller/lists_controller.php">
                <input type="text" name="list" class="textbox" placeholder="New" required> 
                <input type="submit"  name="add" hidden>
                <input type="hidden" name="mode" value="<?=$mode?>">
            </form>

            <?php $nr=0;?>
            
            <!-- Rendering lists -->
            <?php foreach($lists as $list) : ?>
                <?php $name = $list->get_name(); ?>

                <!--Merge conflict dialogue -->
                <?php if(isset($_SESSION["error_msg"]) && $_SESSION["error_msg"]=="List name already in db." &&
                $_SESSION["merge_conflict_list"]==$name ): ?>
                    <span class="errormsg"> Merge lists? </span>
                    <div class="two_inline">
                        <!-- Merge lists -->
                        <a href="../Controller/lists_controller.php?merge&list=<?=$name?>&new=<?=$_SESSION["merge_conflict_new"]?>&mode=<?=$mode?>" 
                        class="btn"> Merge </a>
                        <!-- Don't merge lists -->
                        <a href="../Controller/lists_controller.php?nomerge&mode=<?=$mode?>" 
                        class="btn"> Decline </button>
                    </div>
                <?php endif ?>

                <!-- Modify list name -->
                <form method="get" action="../Controller/lists_controller.php" 
                id="inputarea<?=$nr?>" class="inputarea hide">
                    <!-- Input new name -->
                    <input type="text" class="textbox" placeholder="New name for <?=$name?>" name="new">
                    <input name="list" value="<?=$name?>" hidden>
                    <input name="mode" value="<?=$mode?>" hidden>
                    <!-- Name change options -->
                    <div class="two_inline">
                        <!-- Change name -->
                        <input type="submit" class="btn" name="modify" value="Change">
                        <!-- Don't change name -->
                        <button onclick="decline(this)" value="<?=$nr-1?>" class="btn"> Decline </button>
                    </div>
                </form>

                <!-- Delete list dialogue-->
                <div class="del two_inline hide" id="del<?=$nr++?>">
                    <!-- Delete prompt -->
                    <span class="text">Delete <?=$name?>?</span>
                    <!-- Delete button -->
                    <a href="../Controller/lists_controller.php?list=<?=$name?>&delete&mode=<?=$mode?>" 
                    class="btn red"> Delete </a>
                    <!-- Decline button -->
                    <button onclick="keep(this)" value="<?=$nr-1?>" class="btn"> Keep </button>
                </div>
                
                <div class="line">

                    <!-- Up/down arrows -->
                    <div class="list_name two_options">
                    <?php if($mode == "usdef") : ?>
                        <!-- Up arrow -->
                        <?php if($list != $lists[0]) : ?>
                        <a href="../Controller/lists_controller.php?up&list=<?=$name?>&mode=<?=$mode?>"
                        class="option btn">
                            &#x2191;
                        </a>
                        <?php endif ?>

                        <!-- Down arrow -->
                        <?php if($list != end($lists)) : ?>
                        <a href="../Controller/lists_controller.php?down&list=<?=$name?>&mode=<?=$mode?>"
                        class="option btn">
                            &#x2193;
                        </a>
                        <?php endif ?>
                    <?php endif ?>
                    </div>

                    <!-- List name -->
                    <a href="list.php?list=<?=$name?>" class="list_name
                    <?php if($list->is_starred()) : ?>
                        starred
                    <?php endif ?>">
                        <?= $name ?>
                    </a>
                    
                    <!-- Options -->
                    <div class="three_options">
                        <!-- Delete button -->
                        <button onclick="del(this);" class="option btn red" id="b1" value="<?=$nr-1?>"> 
                            &#x2672;
                        </button>
                        <!-- Name change button -->
                        <button onclick="insert(this);" class="option btn" id="b2" value="<?=$nr-1?>"> 
                            &#x270e;
                        </button>
                        <!-- Star button -->
                        <a href="../Controller/lists_controller.php?star&list=<?=$name?>&mode=<?=$mode?>" class="option btn"> 
                            &#x2605;
                        </a>
                    </div>
                </div>
            <?php endforeach ?>

            <!-- Errors -->
            <span class="errorcls errormsg">
            <?php if(isset($_SESSION["error_msg"])) {
                echo $_SESSION["error_msg"];

                // Delete error messages
                $_SESSION["error_msg"] = null; 
            }?>
            </span>
        </div>
    </body>
</html>