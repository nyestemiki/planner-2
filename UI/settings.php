<?php require_once("../initialization.php"); ?>

<!DOCTYPE HTML>
<html>
    <head>
        <title> Settings </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../main.css">
    </head>
    <body>
        <script>

            // Change username window
            function username() {
                decline();
                document.getElementById("usr").classList.add('show');
            }

            // Change password window
            function password() {
                decline();
                document.getElementById("pwd").classList.add('show');
            }

            // Decline opened windows
            function decline() {
                document.getElementById("pwd").classList.remove('show');
                document.getElementById("usr").classList.remove('show');
                document.getElementById("del").classList.remove('show');
            }

            // Show/hide password
            function show() {

                // Show if hidden
                if(document.getElementById('passwd').type == "password") {
                    document.getElementById('passwd').type = 'text';
                    document.getElementById('showbtn').innerHTML = 'Hide';
                } 
                
                // Hide if shown
                else {
                    document.getElementById('passwd').type = 'password';
                    document.getElementById('showbtn').innerHTML = 'Show';
                }
            }

            // Delete account window
            function delete_account() {
                decline();
                document.getElementById("del").classList.add('show');
            }
        </script>

        <div class="wrapper <?php if(isset($_SESSION["error_msg"])) {
        echo "error";} ?>">

            <div class="menu">
                <!-- Change username -->
                <div class="element">
                    <button onclick="username()" class="btn">Change username</button>
                </div>

                <!-- Change password -->
                <div class="element">
                    <button onclick="password()" class="btn">Change password</button>
                </div>

                <!-- Delete account -->
                <div class="element">
                    <button onclick="delete_account()" class="btn">Delete account</button>
                </div>
            </div>

            <!-- Hidden elements -->

            <!-- Change username -->
            <form method="post" action="../Controller/settings_controller.php" 
            class="inputarea hide" id="usr">
                <input name="usname" hidden>
                <!-- Input new name-->
                <input type="text" name="newname" class="textbox" placeholder="New username" required>
                <!-- Operations -->
                <div class="two_inline">
                    <!-- Change name button -->
                    <input type="submit" value="Change" class="btn" name="submit">
                    <!-- Decline name change button -->
                    <button type="button" onclick="decline();" class="btn">Decline</button>
                </div>
            </form>

            <!-- Change password -->
            <form method="post" action="../Controller/settings_controller.php" 
            class="inputarea hide" id="pwd">
                <input name="pswd" hidden>
                <!-- Input password -->
                <div class="password_area">
                    <!-- Password area -->
                    <input type="password" name="newpass" class="textbox" 
                    placeholder="New password" id="passwd" required>
                    <!-- Show/hide password button -->
                    <button type="button" onclick="show()" class="btn" id="showbtn">Show</button>
                </div>
                <!-- Operations -->
                <div class="two_inline">
                    <!-- Change password -->
                    <input type="submit" value="Change" class="btn">
                    <!-- Decline password change -->
                    <button type="button" onclick="decline()" class="btn">Decline</button>
                </div>
            </form>

            <!-- Delete account -->
            <div class="hide" id="del">
                <!-- Delete prompt -->
                <span class="warning">Are you sure?<br>This step is undoable!</span>
                <!-- Delete account button -->
                <a href="../Controller/settings_controller.php?delete" class="btn delete"> 
                    Delete
                </a>
                <!-- Decline account deletion -->
                <button type="button" onclick="decline();" class="btn">
                    Decline
                </button>
            </div>

            <span class="errorcls errormsg">
            <?php if(isset($_SESSION["error_msg"])) {
                echo $_SESSION["error_msg"];

                // Delete error messages
                $_SESSION["error_msg"] = null; 
            }?>
            </span>
        </div>
    </body>
</html>