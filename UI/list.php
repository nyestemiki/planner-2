<?php require_once("../initialization.php"); ?>

<!DOCTYPE HTML>
<html>
    <head>
        <title> <?=$_REQUEST["list"]?> </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../main.css">
    </head>
    <body>
        <script>
            // Showing order options
            function show() {
                document.getElementById("order-menu").classList.add('show');
                document.getElementById("selected-menu-order").classList.add('hide');
            }
        </script>

        <div class="wrapper">

            <!-- Order option button -->
            <button class="textbox" id="selected-menu-order" onclick="show()"> 
            <?php if(isset($_REQUEST['mode']) && $_REQUEST['mode'] != "" && $_REQUEST['mode'] != "usdef") {
                echo $_REQUEST["mode"];
            } else {
                echo 'Userdefined';
            } ?> 
            </button>

            <!-- Order menu box (open) -->
            <div class="order-menu textbox" id="order-menu">
                <!-- Abc order -->
                <a href="?list=<?=$_REQUEST["list"]?>&mode=abc" class="<?php if(isset($_REQUEST["mode"]) &&
                $_REQUEST["mode"]=='abc') echo 'blue'?>"> 
                    Abc 
                </a>

                <!-- Date order -->
                <a href="?list=<?=$_REQUEST["list"]?>&mode=date" class="<?php if(isset($_REQUEST["mode"]) &&
                $_REQUEST["mode"]=="date") echo 'blue'?>"> 
                    Date 
                </a>

                <!-- Userdefined order -->
                <a href="?list=<?=$_REQUEST["list"]?>" 
                class="<?php if(!isset($_REQUEST["mode"])) echo 'blue'?>"> 
                    Userdefined 
                </a>
            </div>

            <!-- Add new list item -->
            <form method="POST" action="../Controller/list_controller.php">
                <input type="text" class="textbox" placeholder="New" name="element" required>
                <input type="submit" name="add" hidden>
                <input name="list" value="<?=$_REQUEST["list"]?>" hidden>
            </form>

            <!-- Getting the mode -->
            <?php 
                if(isset($_REQUEST['mode']) && $_REQUEST['mode'] != "")
                    $mode=$_REQUEST['mode'];
                else $mode="usdef";
            ?>

            <!-- Getting the loaded list -->
            <?php 
                $listName = $_REQUEST["list"];

                if(isset($_REQUEST["mode"]) && $_REQUEST["mode"]=='abc')
                    $list = get_list_abc($listName);
                else if(isset($_REQUEST["mode"]) && $_REQUEST["mode"]=='date') 
                    $list = get_list_date($listName);
                else
                    $list = get_list_userdefined($listName);
            ?>

            <!-- Rendering list items -->
            <?php foreach($list as $elem) : ?>
                <?php $item = $elem->get_item() ?>
                        
                <div class="line">
                    <div class="list_name two_options">

                    <!-- Up/down arrows -->
                    <?php if($mode == "usdef") : ?>

                        <!-- Up arrow -->
                        <?php if($elem != $list[0]) : ?>
                        <a href="../Controller/list_controller.php?up&element=<?=$elem->get_id()?>&list=<?=$listName?>&mode=<?=$mode?>"
                        class="option btn">
                            &#x2191;
                        </a>
                        <?php endif ?>

                        <!-- Down arrow -->
                        <?php if($elem != end($list)) : ?>
                        <a href="../Controller/list_controller.php?down&element=<?=$elem->get_id()?>&list=<?=$listName?>&mode=<?=$mode?>"
                        class="option btn">
                            &#x2193;
                        </a>
                        <?php endif ?>
                    <?php endif ?>
                    </div>

                    <!-- List item -->
                    <span class="list_name <?php if($elem->is_starred()) : ?>
                        starred
                    <?php endif?>
                    <?php if($elem->is_done()) : ?>
                        done
                    <?php endif?>"> 
                            <?=$item?> 
                    </span>

                    <!-- Option menu -->
                    <div class="three_options">
                        <!-- Delete -->
                        <a href="../Controller/list_controller.php?element=<?=$elem->get_id()?>&list=<?=$listName?>&delete&mode=<?=$mode?>"
                        class="option btn red"> 
                            &#x2672;
                        </a>
                        <!-- Star -->
                        <a href="../Controller/list_controller.php?element=<?=$elem->get_id()?>&list=<?=$listName?>&star&mode=<?=$mode?>" 
                        class="btn option">
                            &#9733; 
                        </a>
                        <!-- Done -->
                        <a href="../Controller/list_controller.php?element=<?=$elem->get_id()?>&list=<?=$listName?>&state&mode=<?=$mode?>" 
                        class="btn option">
                            &#10004;
                        </a>
                    </div>
                </div>
                
            <?php endforeach ?>

            <!-- Errors -->
            <span class="errorcls errormsg">
            <?php if(isset($_SESSION["error_msg"])) {
                echo $_SESSION["error_msg"];

                // Deleting error messages
                $_SESSION["error_msg"] = null; 
            }?>
            </span>
        </div>
    </body>
</html>