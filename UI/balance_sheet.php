<?php require_once("../initialization.php"); ?>

<!DOCTYPE HTML>
<html>
    <head>
        <title><?=$_REQUEST["category"]?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../main.css">
    </head>
    <body>
        <div class="wrapper">
            <!-- Adding new purchase -->
            <form method="POST" action="../Controller/balance_sheet_controller.php">
                <div class="two_inline item_price">
                    <!-- Purchase name -->
                    <input type="text" placeholder="New" name="content" required>
                    <!-- Purchase price -->
                    <input type="text" placeholder="$" name="price" required>
                </div>
                <!-- Add purchase button -->
                <input type="submit" name="add" hidden>
                <!-- Category -->
                <input type="hidden" name="category" value="<?=$_REQUEST["category"]?>">
            </form>

            <!-- Purchases -->
            <?php foreach(load_balance_sheet($_REQUEST["category"]) as $element) : ?>
            <!-- One purchase -->
            <div class="two_inline balance_line">
                <!-- Name of purchase -->
                <span class="white_font"><?=$element->get_content()?></span>
                <!-- Price and delete button -->
                <div class="ops">
                    <!-- Price -->
                    <span class="white_font ops_scr"><?=$element->get_price()?></span>
                    <!-- Delete button -->
                    <a href="../Controller/balance_sheet_controller.php?delete&category=<?=$_REQUEST['category']?>&content=<?=$element->get_content()?>&price=<?=$element->get_price()?>" 
                    class="ops_del hide"> Delete </a>
                </div>
            </div>
            <?php endforeach ?>
            
            <!-- Errors -->
            <span class="errorcls errormsg">
            <?php if(isset($_SESSION["error_msg"])) {
                echo $_SESSION["error_msg"];

                // Deleting error messages
                $_SESSION["error_msg"] = null; 
            }?>
            </span>
        </div>
    </body>
</html>