<?php require_once("../initialization.php"); ?>

<!DOCTYPE HTML>
<html>
    <head>
        <title> Timetable </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../main.css">
    </head>
    <body onload="mode()">
        <script>

            // Toggle area modification
            function modify(element) {
                var name = element.id.slice(3);
                if (element.value == "") {
                    element.value = '';
                    document.getElementById("b//"+name).innerHTML = ".";
                } else {
                    document.getElementById("b//"+name).innerHTML = element.value;
                }
            }

            // Show textarea
            function show(element) {
                var name = element.id.slice(3);
                document.getElementById("t//"+name).classList.add('show');
                document.getElementById("b//"+name).classList.add('hide');
            }

            // Show button
            function hide(element) {
                var name = element.id.slice(3);
                document.getElementById("t//"+name).classList.remove('show');
                document.getElementById("b//"+name).classList.remove('hide');
            }

            // Store data
            function save(element) {

                // Asyncronous operation
                xmlhttp = new XMLHttpRequest();

                // Stores data in database
                xmlhttp.open("GET", "../Ajax/timetable_save.php?content="+element.value+"&id="+element.id.slice(3), true);
                xmlhttp.send();
            }

            let toggle = false;

            // Toggle edit/done mode
            function mode() {
                var x = document.getElementById("tt_content").querySelectorAll(".btn");

                // Change to edit mode if in done mode
                if(toggle) {
                    for(var i=0; i<60; i++) {
                        if(x[i].innerHTML==".") {
                            x[i].classList.remove("hide");
                        }
                    }
                    document.getElementById("edit").innerHTML="Done";
                    toggle = false;
                } 
                
                // Change to done mode if in edit mode
                else {
                    for(var i=0; i<60; i++) {
                        if(x[i].innerHTML==".") {
                            x[i].classList.add("hide");
                        }
                    }
                    document.getElementById("edit").innerHTML="Edit";
                    toggle = true;
                }
            }
        </script>

        <div class="wrapper">
            <!-- Mode button -->
            <button onclick="mode()" class="btn" id="edit">Edit</button>

            <!-- Timetable -->
            <div class="timetable">
                <!-- Header -->
                <div class="days">
                    <span>Time</span>
                    <span>Monday</span>
                    <span>Tuesday</span>
                    <span>Wednesday</span>
                    <span>Thursday</span>
                    <span>Friday</span>
                </div>
                <!-- Hours -->
                <div class="times">
                <?php for($a=8; $a<20; $a++) : ?>
                    <span> <?=$a?> - <?=$a+1?> </span>
                <?php endfor ?>
                </div>

                <!-- Getting timetable content -->
                <?php $content = load_timetable() ?>

                <div class="timetablecontent" id="tt_content">
                <?php for($i=1; $i<=60; $i++) : ?>
                <div> 
                    <!-- Modify -->
                    <textarea class="textbox hide small-box" onchange="modify(this); hide(this); save(this)" id='t//<?=$i?>'>
                    <?php 
                        if(array_key_exists($i, $content) && ($content[$i]!="" || $content[$i]!=".")) 
                            echo $content[$i];
                        else echo '';
                    ?>
                    </textarea>

                    <!-- Event -->
                    <button class="btn" id='b//<?=$i?>' onclick="show(this)"><?php if(array_key_exists($i, $content) && $content[$i]!="")
                        echo $content[$i];
                    else echo '.';
                    ?></button>
                </div>
                <?php endfor ?>
                </div>
            </div>
        </div>
    </body>
</html>