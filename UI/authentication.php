<?php 
    // Access is granted to all user's
    $GUEST_ACCESS = true;
    $page = "auth";
    require_once("../initialization.php"); 
?>

<!DOCTYPE HTML>
<html>
    <head>
        <title> Authentication </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../main.css">
    </head>
    <body>
        <script>
            // Shows/hides password
            function show() {
                if(document.getElementById('passwd').type == "password") {
                    document.getElementById('passwd').type = 'text';
                    document.getElementById('showbtn').innerHTML = 'Hide';
                } else {
                    document.getElementById('passwd').type = 'password';
                    document.getElementById('showbtn').innerHTML = 'Show';
                }
            }

            // Checks if input is valid
            function validate() {
                const username = document.getElementById("username").value;
                const password = document.getElementById("passwd").value;

                // Blank inputs are not tolerated
                if(!username || !password) {
                    document.getElementById("error").classList.remove("hide");
                    document.getElementById("error").innerHTML = 'No blank inputs'; 
                    setTimeout(function(){
                        document.getElementById("error").classList.add("hide");
                    }, 3000);
                    return;
                }

                // Asyncronous operations
                xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if(this.readyState == 4 && this.status == 200) {
                        
                        // Logging in if no errors occurred
                        if(this.responseText == 'true') {
                            window.location = "../Controller/authentication_controller.php?login&username="+username+"&password="+password;
                        } 
                        
                        // Handling errors
                        else {
                            document.getElementById("error").classList.remove("hide");
                            document.getElementById("error").innerHTML = 'No such record'; 
                            setTimeout(function(){
                                document.getElementById("error").classList.add("hide");
                            }, 3000);
                        }
                    }
                };
                
                xmlhttp.open("POST", "../Ajax/login_validator.php?username="+username+"&password="+password, true);
                xmlhttp.send();
            }

            // Checks if the register input data is valid
            function register() {
                const username = document.getElementById("username").value;
                const password = document.getElementById("passwd").value;
                
                // Blank inputs are not tolerated
                if(!username || !password) {
                    document.getElementById("error").classList.remove("hide");
                    document.getElementById("error").innerHTML = 'No blank inputs'; 
                    setTimeout(function(){
                        document.getElementById("error").classList.add("hide");
                    }, 3000);
                    return;
                }

                // Asyncronous operations
                xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if(this.readyState == 4 && this.status == 200) {

                        // Registering if no error occured
                        if(this.responseText == 'true') {
                            window.location = "../Controller/authentication_controller.php?register&username="+username+"&password="+password;
                        } 
                        
                        // Handling errors
                        else {
                            document.getElementById("error").classList.remove("hide");
                            document.getElementById("error").innerHTML = 'Username taken'; 
                            setTimeout(function(){
                                document.getElementById("error").classList.add("hide");
                            }, 3000);
                        }
                    }
                };

                xmlhttp.open("POST", "../Ajax/register_validator.php?username="+username+"&password="+password, true);
                xmlhttp.send();
            }
        </script>
        
        <div class="wrapper">
            <!-- Username input area -->
            <textarea class="textbox" placeholder="Username" id="username"></textarea>
            <!-- Password input area -->
            <div class="password_area">
                <!-- Password -->
                <input type="password" class="textbox" placeholder="Password" id="passwd">
                <!-- Show button for password -->
                <button onclick="show()" class="btn" id="showbtn">Show</button>
            </div>
            <!-- Login button -->
            <button onclick="validate()" class="btn">Log in</button>
            <!-- Register button -->
            <button onclick="register()" class="btn">Register</button>
            <!-- Errors -->
            <span class="errorcls errormsg error hide" id="error"></span>
        </div>
    </body>
</html>