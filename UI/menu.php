<?php require_once("../initialization.php"); ?>

<!DOCTYPE HTML>
<html>
    <head>
        <title> Menu </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../main.css">
    </head>
    <body>
        <div class="wrapper">
            <div class="menu">

                <!-- Lists -->
                <div class="element">
                    <a href="lists.php" class="btn">Lists</a>
                </div>

                <!-- Weather -->
                <div class="element">
                    <a href="weather.php" class="btn">Weather</a>
                </div>

                <!-- Timetable -->
                <div class="element">
                    <a href="timetable.php" class="btn">Timetable</a>
                </div>

                <!-- Subject -->
                <div class="element">
                    <a href="subject.php" class="btn">Notes</a>
                </div>

                <!-- Balance -->
                <div class="element">
                    <a href="balance.php" class="btn">Balance</a>
                </div>

                <!-- Pomodoro -->
                <div class="element">
                    <a href="pomodoro.php" class="btn">Pomodoro</a>
                </div>

                <!-- Settings -->
                <div class="element">
                    <a href="settings.php" class="btn">Settings</a>
                </div>
                
                <!-- Logout button -->
                <div class="element">
                    <a href="../Controller/authentication_controller.php?logout=" class="btn">Log Out</a>
                </div>

            </div>
        </div>
    </body>
</html>