<?php require_once("../initialization.php"); ?>

<!DOCTYPE HTML>
<html>
    <head>
        <title> Pomodoro </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../main.css">
    </head>
    <body>
        <script>
            var time = 1500; // Time for work (25 minutes)
            var break_time = 300; // 5 minutes

            // Sets work timer
            function pomodoro() {
                var timer = setInterval(function(){ 
                    var m = Math.floor(time/60); // minutes
                    var s = time%60; // seconds
                    time--;

                    if(s < 10) {
                        document.getElementById("timer").innerHTML = m+":0"+s;
                    } else {
                        document.getElementById("timer").innerHTML = m+":"+s;
                    }
                    
                    // Altering progressbar
                    document.getElementById("progressbar").style.backgroundColor = "blue";
                    document.getElementById("progressbar").style.width = 100-time*0.066+"%";
                        
                    // Stop condition
                    if(time < 0) {
                        document.getElementById("timer").innerHTML = "<button onclick='break_timer()' class='btn'>Take the break</button>";
                        document.getElementById("progressbar").style.backgroundColor = "transparent";
                        clearInterval(timer);
                    }   
                }, 1000);
            }

            // Sets break timer
            function break_timer() {
                var timer = setInterval(function(){ 
                    var m = Math.floor(break_time/60); //minutes
                    var s = break_time%60; //seconds
                    break_time--;

                    if(s < 10) {
                        document.getElementById("timer").innerHTML = m+":0"+s;
                    } else {
                        document.getElementById("timer").innerHTML = m+":"+s;
                    }

                    // Altering progressbar
                    document.getElementById("progressbar").style.backgroundColor = "blue";
                    document.getElementById("progressbar").style.width = 100-break_time*0.333+"%";
                        
                    // Stop condition
                    if(break_time < 0) {
                        document.getElementById("timer").innerHTML = "<button onclick='pomodoro()' class='btn'>Continue work</button>";

                        time = 1501; // Resetting work time
                        break_time = 301; // Resetting break time

                        document.getElementById("progressbar").style.backgroundColor = "transparent"; // Resetting progressbar
                        
                        clearInterval(timer);
                    }   

                }, 1000);
            }
        </script>

        <div class="wrapper">
        <div id="progressbar">
            <button id="timer" class="btn" onclick="pomodoro()">
                Start Pomodoro
            </button>
        </div>
        </div>
    </body>
</html>