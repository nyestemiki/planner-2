<?php require_once("../initialization.php"); ?>

<!DOCTYPE HTML>
<html>
    <head>
        <title> subject </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../main.css">
    </head>
    <body>
        <script>
            
            // Show add note window
            function show(id) {
                document.getElementById(id).classList.add('show');
                document.getElementById('b//'+id).classList.add('hide');
            }

            // Show delete confirm dialogue
            function del_confirm(subject) {
                document.getElementById('d//'+subject).classList.add('show');
            }

            // Hide delete confirm dialogue 
            function decline(subject) {
                document.getElementById('d//'+subject).classList.remove('show');
            }

            // Show average of a subject
            function average(element) {
                const subject = element.slice(3);

                // Asyncronous operations
                xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {

                        // Rendering the average
                        document.getElementById("ide").innerHTML = this.responseText;
                    }
                };

                // Getting average for a certain subject
                xmlhttp.open("GET", "../Ajax/average.php?subject="+subject, true);
                xmlhttp.send();
            }

            // Hide edit dots when window is rendered
            window.onload = function() {

                // Asyncronous operations
                xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if(this.readyState == 4 && this.status == 200) {
                        this.responseText.split(';').forEach(function(item) {
                            document.getElementById('a//'+item).innerHTML='';
                        });
                    }
                };

                // Getting empty areas
                xmlhttp.open("GET", "../Ajax/has_note.php", true);
                xmlhttp.send();
            };
        </script>

        <div class="wrapper <?php if(isset($_SESSION["error_msg"])) {
        echo "error";} ?>">
            
            <!-- Add new subject -->
            <form method="POST" action="../Controller/subjects_controller.php">
                <input type="text" name="subject" class="textbox" placeholder="New subject" required>
                <input type="submit" name="new_subject" hidden>
            </form>

            <!-- Place for average -->
            <div id="ide" class="msg"></div>

            <?php $i=0 ?>

            <!-- Rendering the subjects -->
            <?php foreach(load_subjects() as $subject) : ?>
            <!-- Subject and its notes -->
            <div class="two_inline notes_container">
                <!-- Subject name AND delete button -->
                <button onclick="del_confirm(this.id)" id="<?= $subject ?>" class="subject_link">
                    <?=$subject?>
                </button>
                <!-- Delete subject -->
                <div class="del_div hide" id="d//<?=$subject?>">
                    <!-- Delete propmt -->
                    <span class="msg">Are you sure?</span>
                    <!-- Confirm delete -->
                    <a href="../Controller/subjects_controller.php?delete_subject&subject=<?=$subject?>" 
                    class="subject delete">Delete subject</a>
                    <!-- Decline delete -->
                    <button onclick="decline(this.id)" id="<?=$subject?>" class="subject_link"> Decline </button>
                </div>
                <!-- Renderig notes -->
                <div class="notes">
                    <?php foreach(load_notes($subject) as $note) : ?>
                    <!-- Note AND delete button -->
                    <?php if($note!="") : ?>
                    <a href="../Controller/subjects_controller.php?delete_note&note=<?=$note?>&subject=<?=$subject?>" class="note"> 
                        <?=$note?> 
                    </a>
                    <?php endif ?>
                    <?php endforeach ?>
                    <!-- New note button -->
                    <button onclick="show(<?=$i?>)" class="note_add" id="b//<?=$i?>"> + </button>
                    <!-- New note input -->
                    <form action="../Controller/subjects_controller.php" method="post" class="hide" id="<?=$i?>">
                        <input type="text" name="note" class="textbox small_text nomargin">
                        <input type="submit" name="addnote" hidden>
                        <input type="hidden" name="subject" value="<?=$subject?>">
                    </form>
                    <!-- Average -->
                    <button onclick="average(this.id)" id="a//<?=$subject?>" class="note_add"> Average </button>

                    <?php $i++ ?>
                </div>
            </div>
            <?php endforeach ?>
            
            <!-- Errors -->
            <span class="errorcls errormsg">
            <?php if(isset($_SESSION["error_msg"])) {
                echo $_SESSION["error_msg"];

                // Delete error messages
                $_SESSION["error_msg"] = null; 
            }?>
            </span>
        </div>
    </body>
</html>